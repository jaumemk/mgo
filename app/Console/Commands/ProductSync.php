<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Product;
use App\Models\Promotion;
use App\Models\City;
use App\Models\Type;
use Illuminate\Support\Str;

class ProductSync extends Command
{

    protected $products;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshing products from the XML File to App database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct() 
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Executing service connector...');
        
        $url = env('PRODUCTS_XML_PATH', public_path('service/500212.xml'));

        $this->line('Getting XML File contents:' . $url);
        // parsing file
        $fileContents = file_get_contents($url);
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml = simplexml_load_string($fileContents, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($simpleXml);
        $object = json_decode($json);
        $inmuebles = collect($object->Inmuebles->Inmueble);

        $this->refresh($inmuebles);
    }

    public function refresh($inmueble)
    {
        
        $this->line('Truncating products table.');
        Product::query()->truncate();
        $this->line('Truncating cities table.');
        City::query()->truncate();
        $this->line('Truncating type table.');
        Type::query()->truncate();
        $this->line('Truncating promotions table.');
        Promotion::query()->truncate();

        // insert products
        $this->line("Inserting " . $inmueble->count() . " products to local database.");
        $bar = $this->output->createProgressBar($inmueble->count());

        $bar->start();
        $inmueble->each(function($item, $key) use ($bar){

            $city_id = \App\Models\City::firstOrCreate([
                'label' => $item->NombreMunicipio,
                'slug' => Str::slug($item->NombreMunicipio, '-')
            ]);

            $type_id = \App\Models\Type::firstOrCreate([
                'label' => $item->LanguageData->Language[1]->txt_NombreTipoInmueble,
                'slug' => Str::slug($item->LanguageData->Language[1]->txt_NombreTipoInmueble, '-')
            ]);

            $extras = [];

            foreach(config('site.extras') as $key => $extra){
                if(property_exists($item, $extra[0]))
                {
                    $extras[$key] =  (bool) $item->{$extra[0]};
                }
            }

            $title = $item->LanguageData->Language[1]->txt_NombreTipoInmueble . ' ' . $item->LanguageData->Language[1]->txt_Estado;

            $product = [
                'mode' => $item->LanguageData->Language[1]->txt_NombreOperacion == 'en venda'? 'buy' : 'rent',
                'city_id' => $city_id->id,
                'type_id' => $type_id->id,
                'title' => $title,
                'slug' => Str::slug( $title . ' a ' . $city_id->label , '-'),
                'highlighted' => $item->Destacado,
                'extras' => json_encode($extras),
                'external_src_reference' => $item->NumeroExpediente,
                'external_src_rooms' => $item->TotalHab,
                'external_src_bathrooms' => $item->TotalBanos,
                'external_src_area' => $item->SuperficieTotal,
                'external_src_price' => $item->Precio1Euros,
                'external_src_object' => json_encode($item)
            ];

            if(substr($item->NumeroExpediente, 0, 2) == 'PR'){
                $promotion = Promotion::updateOrCreate(
                    ['ref' => mb_substr($item->NumeroExpediente, 0, -3)],
                    []
                );

                $product['promotion_id'] = $promotion->id;
            }

            $product = \App\Models\Product::create($product);
                
            $bar->advance();
        });

        $bar->finish();
        $this->line('');

        $cities = \App\Models\City::count();
        $this->line('And ' . $cities . ' related cities inserted.');
        $this->line('');

        $this->line('Refresh completed!');
        
        return \App\Models\Product::all();
    }

}
    
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('product', 'products');
        CRUD::denyAccess(['create', 'update', 'delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        //CRUD::column('id');
        CRUD::column('mode')->label('s/r');
        CRUD::column('highlighted')->label('*');
        CRUD::column('external_src_reference')->label('REF');
        CRUD::column('title');
        CRUD::column('city_id')->type('relationship')->name('city_id')->label('City');
        CRUD::column('external_src_price')->label('Price');
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
        CRUD::column('external_src_area')->label('Area');
        CRUD::column('external_src_rooms')->label('Rooms');
        CRUD::column('external_src_bathrooms')->label('Bathroom');
        CRUD::column('extras')->type('json')->label('Extras');
        //CRUD::column('promotion_id')->type('relationship')->name('promotion_id')->label('City');
        CRUD::column('external_src_object')->type('json')->label('Original source');
    }
}

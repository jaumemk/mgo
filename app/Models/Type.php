<?php

namespace App\Models;


use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Type extends Model
{
    use CrudTrait;

    protected $table = 'types';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $hidden = [];
    // protected $dates = [];


    protected $fillable = [
        'label',
        'slug',
    ];

    /**
     * Attribute shown on the element to identify this model.
     *
     * @var string
     */
    protected $identifiableAttribute = 'label';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('ordered', function (Builder $builder) {
            $builder->orderBy('label');
        });
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function identifiableAttribute()
    {
        // process stuff here
        return $this->identifiableAttribute;
    }

}

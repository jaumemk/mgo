<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Hamcrest\Type\IsString;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $hidden = [];
    // protected $dates = [];


    protected $casts = [
        'external_src_object' => 'object'
    ];

    protected $fillable = [
        'mode',
        'city_id',
        'type_id',
        'title',
        'slug',
        'highlighted',
        'extras',
        'description',
        'external_src_reference',
        'external_src_price',
        'external_src_area',
        'external_src_rooms',
        'external_src_bathrooms',
        'external_src_object',
        'promotion_id',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function getPreTitleAttribute()
    {
        return json_decode($this->external_src_object)->LanguageData->Language[1]->txt_Cabecera1;
    }

    public function getDetailsAttribute()
    {
        return json_decode($this->external_src_object)->LanguageData->Language[1]->txt_Detalle1;
    }

    public function getImagesAttribute()
    {
        $images = json_decode($this->external_src_object)->Medias->Fotos->Foto;
        
        if(is_string($images)) {
            return array($images);
        }

        return $images;
    }

    public function getExternalTxt($prop)
    {
        if(
            isset(json_decode($this->external_src_object)->LanguageData->Language[1]->{"txt_$prop"})
            &&
            is_string(json_decode($this->external_src_object)->LanguageData->Language[1]->{"txt_$prop"})
            )
        {
            return json_decode($this->external_src_object)->LanguageData->Language[1]->{"txt_$prop"};
        }

        return null;
    }

    public function getImageAttribute()
    {
        return $this->images[0];
    }

    public function getVideoAttribute()
    {
        // checks exostence of video URL and parses
        // the VIDEO_ID parameter to convert conventional
        // YouTube URL to embedable

        if(isset(json_decode($this->external_src_object)->Medias->Videos->Video)){
            if(isset(parse_url(json_decode($this->external_src_object)->Medias->Videos->Video)['query'])) {
                $query = parse_url(json_decode($this->external_src_object)->Medias->Videos->Video)['query'];

                parse_str($query, $params);

                if(isset($params['v'])) {
                    return 'https://www.youtube.com/embed/'.$params['v'];
                }
            }
        }

        return false;
    }


}

<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'promotions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];


    public function getNumberAttribute()
    {
        return mb_substr(strrev($this->ref), 0, -2);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getLocationAttribute()
    {
       return $this->products()->get()->firstOrFail()->city()->first()->label;
    }


    public function getWcAttribute()
    {
        if($this->products()->get()->min('external_src_bathrooms') == $this->products()->get()->max('external_src_bathrooms')) {
            return $this->products()->get()->min('external_src_bathrooms');
        }
        return $this->products()->get()->min('external_src_bathrooms') .'-'. $this->products()->get()->max('external_src_bathrooms');
    }


    public function getAreaAttribute()
    {
        if($this->products()->get()->min('external_src_area') == $this->products()->get()->max('external_src_area')) {
            return round($this->products()->get()->min('external_src_area'));
        }
        return round($this->products()->get()->min('external_src_area')) .'-'. round($this->products()->get()->max('external_src_area'));
    }

    public function getRoomsAttribute()
    {
        if($this->products()->get()->min('external_src_rooms') == $this->products()->get()->max('external_src_rooms')) {
            return $this->products()->get()->min('external_src_rooms');
        }
        return $this->products()->get()->min('external_src_rooms') .'-'. $this->products()->get()->max('external_src_rooms');
    }

    public function getImageAttribute()
    {
        return $this->products()->orderBy('external_src_area', 'desc')->get()->first()->image;
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

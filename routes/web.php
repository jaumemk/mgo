<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $promotions = \App\Models\Promotion::orderBy('ref', 'desc')->get();

    $promotions = $promotions->filter(function ($promo, $key) {
        return $promo->products()->get()->count() > 1;
    })->take(2);

    return view('pages/home', compact('promotions'));

})->name('home');

Route::get('promocions', function () {

    $promotions = \App\Models\Promotion::orderBy('ref', 'desc')->get();

    return view('pages/promotions', compact('promotions'));

})->name('promotions');

Route::get('contacte', function () {

    return view('pages/contact');

})->name('contact');

Route::get('nosaltres', function () {
    
    return view('pages/about-us');

})->name('about-us');

Route::get('filtre', function () {

    $s = request()->all();

    $q = \App\Models\Product::where('mode', '=', (isset($s['mode'])) ? 'rent' : 'buy');

    if(isset($s['ref']))
    {
        $q = $q->where('external_src_reference', 'LIKE', '%' . $s['ref'] . '%');
    }

    if(isset($s['cities']) && count($s['cities']) > 0)
    {
        $q = $q->whereIn('city_id', array_map('intval', $s['cities']));
    }

    if(isset($s['types']) && count($s['types']) > 0)
    {
        $q = $q->whereIn('type_id', array_map('intval', $s['types']));
    }

    if(isset($s['extras']) && count($s['extras']) > 0)
    {
        foreach($s['extras'] as $extra)
        {
            if(in_array($extra, array_keys(config('site.extras'))))
            {
                $q = $q->where('extras->'. $extra , true);
            }
        }        
    }

    if(isset($s['option-room']))
    {
        $min = (int) explode('-', $s['option-room'])[1];
        $q = $q->where('external_src_rooms', '>=', $min);
    }

    if(isset($s['option-bathroom']))
    {
        $min = (int) explode('-', $s['option-bathroom'])[1];
        $q = $q->where('external_src_bathrooms', '>=', $min);
    }

    $results = $q->get();

    return view('pages/results', ['results' => $results]);

})->name('filter');

Route::get('analyze', function () {
    
    Artisan::call('product:refresh');

    // parsing file
    $url = env('PRODUCTS_XML_PATH', public_path('service/500212.xml'));
    $fileContents = file_get_contents($url);
    $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
    $fileContents = trim(str_replace('"', "'", $fileContents));
    $simpleXml = simplexml_load_string($fileContents, "SimpleXMLElement", LIBXML_NOCDATA);
    $json = json_encode($simpleXml);
    $object = json_decode($json);
    $inmuebles = collect($object->Inmuebles->Inmueble);

    dump($inmuebles);

});

Route::get('test', function () {
    
    dump(App\Models\Promotion::all()->last()->image);

});


// keep this route last

Route::get('/{slug}/{ref}', function ($slug, $ref) {

    $product = \App\Models\Product::where('external_src_reference', $ref)->firstOrFail();

    if($slug != $product->slug)
        return redirect(route('product', ['slug' => $product->slug, 'ref' => $ref]));

    return view('pages/product', compact('product'));

})->name('product');

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="{{ strip_tags(__('main.meta-description')) }}">
    <meta name="keywords" content="{{ strip_tags(__('main.meta-keywords')) }}">
    <title>@yield('title') {{ strip_tags(__('main.meta-title')) }}</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>

    @include('parts/navigation')

    <main id="main-content">
        @yield('main-content')
    </main>

    <div id="full-search" class="d-flex flex-column justify-content-between bg-gray">
        <div class="text-center pt-3">
            <span class="font-brand-alt font-brand-alt-1">Tancar</span>
            <a href="#" id="close-full-search" class="brand-close"></a>
        </div>
        <div class="search-module">
            @include('sections/search', ['parent_id' => 'full-search'])
        </div>
        <div class="p-lg-8">
            <!-- hidden div -->
        </div>
    </div>

    <footer>
        <div id="footer" class="container py-5">
            <div class="row no-gutters">
                <div class="col-12 col-lg-3">
                    <h2 class="font-brand-alt font-brand-alt-1 font-weight-bold mb-2">
                        Contacta'ns
                    </h2>

                    <script type="text/javascript">
                        emailE = ('hola@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>
                    <br>
                    <a href="tel:+34938866165" class="a-clean">t 938 866 165</a>
                </div>
                <div class="col-12 col-lg-3 mt-4 mt-lg-0">
                    <h2 class="font-brand-alt font-brand-alt-1 font-weight-bold mb-2">
                        Visita'ns
                    </h2>
                    <address class="m-0">
                        <a href="https://www.google.es/maps/place/Carrer+de+Jacint+Verdaguer,+9,+08500+Vic,+Barcelona/" target="_blank" class="a-clean">
                            C. Jacint Verdaguer 9, baixos B, Vic
                        </a>
                    </address>
                    Dl a Div — De 9.30h a 19.30h <br>
                    Ds — De 9h a 12h
                </div>
                <div class="col-12 col-lg-1"></div>
                <div class="col-12 col-lg-2">
                    <br>
                    <div class="mb-2"></div>
                    <div>
                        <h2 class="font-brand-alt font-brand-alt-1 font-weight-bold">
                            <a href="#" class="hover-underline-remove">Avís legal</a>
                        </h2>
                        <h2 class="font-brand-alt font-brand-alt-1 font-weight-bold">
                            <a href="#" class="hover-underline-remove">Política de cookies</a>
                        </h2>
                        <h2 class="font-brand-alt font-brand-alt-1 font-weight-bold">
                            <a href="#" class="hover-underline-remove">Site by Suki</a>
                        </h2>
                    </div>
                </div>
                <div class="col-12 col-lg-2">
                    <br>
                    <div class="mb-2"></div>
                    <img src="{{ asset('images/rai-cat.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-12 col-lg-1 text-center font-brand-alt font-brand-alt-1 font-weight-bold">
                    <a href="#" class="a-clean">
                        <div class="arrow-up"></div>
                        Amunt
                    </a>
                </div>
            </div>
        </div>
        <div class="py-5"></div>
    </footer>

    <div id="social-float" class="py-2 d-flex justify-content-centera align-items-center">
        <div class="px-05">
            <a class="social-circle" href="https://www.instagram.com/mgosona/" target="_blank" role="button">
                <div class="icon icon-bg-secondary">
                    @php include(public_path('images/icons/icon-instagram.svg')) @endphp
                </div>
            </a>
        </div>
        <div class="px-05">
            <a class="social-circle social-circle-big" id="show-full-search" href="#full-search" role="button">
                <div class="icon">
                    @php include(public_path('images/icons/icon-search.svg')) @endphp
                </div>
            </a>
        </div>
        <div class="px-05">
            <a class="social-circle" href="https://api.whatsapp.com/send?phone=+34938866165&text=Hola!" target="_blank" role="button">
                <div class="icon">
                    @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                </div>
            </a>
        </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

</body>

</html>

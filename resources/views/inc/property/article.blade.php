<article class="property">
    <div class="btn-override text-center">
        <a href="{{ route('product', ['slug' => $article->slug, 'ref' => $article->external_src_reference]) }}">
            <div class="image rounded-sm" style="background-image: url({{ $article->image ?? asset('images/1920x1080.png') }})">
                <img src="{{ asset('images/1920x1080.png') }}" class="img-fluid" alt="">
            </div>
            <span class="btn {{ $class ?? 'btn-secondary' }} translate-m50 btn-override">Més informació</span>
        </a>
    </div>
    <div class="text-center">

        <h1 class="h5 font-weight-bold pt-1 pb-3">{{ $article->title . ' — ' . $article->city()->first()->label }}</h1>

        <div class="row no-gutters font-brand-alt font-brand-alt-1">
            <div class="col">
                Ref.
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $article->external_src_reference }}</span>
            </div>
            <div class="col border-left border-primary">
                M<sup class="small font-weight-bold">2</sup>
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $article->external_src_area }}</span>
            </div>
            <div class="col border-left border-primary">
                Hab.
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $article->external_src_rooms }}</span>
            </div>
            <div class="col border-left border-primary">
                WC
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $article->external_src_bathrooms }}</span>
            </div>
        </div>
    </div>
</article>

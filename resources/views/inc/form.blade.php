<div class="text-center">
    <h3 class="h4 font-weight-bold mb-4">Sol·licita’ns informació</h3>
</div>
<div class="pb-4">
    <div class="row justify-content-center text-center">
        <div class="col col-lg-4">
            <p>
                Has trobat el que buscaves? Necessites més informació? No ho dubtis i posa’t en contacte amb nosaltres trucant-nos al telèfon 938 866 165 o bé a través del següent formulari:
            </p>
        </div>
    </div>
</div>

<form action="#" class="input-transparent">
    <div class="row no-gutters justify-content-center">
        <div class="col-12 col-lg-4 pr-lg-1">
            <div class="form-group">
                <select class="custom-select">
                    <option selected>Què desitges?</option>
                    @foreach ($options as $key => $option)
                        <option value="{{ $key }}">{{ $option }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Correu electrònic" name="email">
            </div>
        </div>
        <div class="col-12 col-lg-4 pl-lg-1">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nom i cognoms" name="name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Telèfon" name="phone">
            </div>
        </div>
    </div>

    <div class="row no-gutters justify-content-center">
        <div class="col-12 col-lg-8">
            <div class="form-group">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" placeholder="Deixa'ns el teu missatge"></textarea>
            </div>
        </div>
    </div>

    <div class="row no-gutters justify-content-center pt-2 pt-lg-4">
        <div class="col-12 col-lg-4 font-ttnorms">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="accept-terms">
                <span class="custom-control-label small"><label for="accept-terms">Accepto els <a href="#" class="a-clean"><u>termes i condicions</u></a></label></span>
            </div>

        </div>
        <div class="col-12 col-lg-4 text-center text-lg-right mt-2 mt-lg-0">
            <button type="submit" class="btn btn-primary btn-override-2"> Enviar consulta </button>
        </div>
    </div>


</form>

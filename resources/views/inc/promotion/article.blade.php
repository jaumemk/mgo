<article>

    <div class="d-flex justify-content-between">
        <div>
            <h1 class="pb-2 display display-4">{{ $promotion->location }}</h1>
        </div>
        <div class="promo-num">
            <div class="font-brand-alt font-brand-alt-1">promo</div>
            <div class="font-weight-bold display-2">{{ $promotion->number }}</div>
        </div>
    </div>

    <div class="btn-override text-center">
        <a href="{{ route('filter') }}?ref={{ $promotion->ref }}"><img src="{{ $promotion->image }}" class="img-fluid rounded-sm" alt=""></a>
        <a href="{{ route('filter') }}?ref={{ $promotion->ref }}" class="btn btn-secondary translate-m50">Més informació</a>
    </div>
    <div class="text-center">
        <div class="row pt-1 font-brand-alt font-brand-alt-1">
            <div class="col">
                Ref.
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $promotion->ref }}</span>
            </div>
            <div class="col border-left border-primary">
                M<sup class="small font-weight-bold">2</sup>
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $promotion->area }}</span>
            </div>
            <div class="col border-left border-primary">
                Hab.
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $promotion->rooms }}</span>
            </div>
            <div class="col border-left border-primary">
                WC
                <br>
                <span class="d-block font-brand-alt-2 mt-1">{{ $promotion->wc }}</span>
            </div>
        </div>

        <div class="pt-4">
            <p>
                Informa't de les nostres promocions residencials!
            </p>
        </div>
    </div>
</article>

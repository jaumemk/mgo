@extends('layouts.main')
@section('title', 'Contacte |')
@section('main-content')
<div class="bg-secondary rounded-top-lg py-5">
    <div class="container text-center">

        <h1 class="text-white display-4">Sempre a punt <br> per obrir—vos la porta</h1>

        <div class="row justify-content-center">
            <div class="col col-lg-5">
                <div class="py-3">
                    <p>
                        MGO t’ho posa fàcil! <br>
                        Situats al centre de Vic, vine a visitar-nos quan vulguis a les nostres oficines. El nostre equip t’escoltarà, t’assessorà i t’acompanyarà en tots els processos de compra, venda o lloguer d’un immoble.
                    </p>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-12 col-lg-3">
                <span class="h5 font-weight-bold">Dl a Dv <br>
                    9.30h a 19.30h
                </span>
            </div>
            <div class="col-12 d-lg-none">
                <div class="border-white mt-2 mb-2 border-top"></div>
            </div>
            <div class="col-12 col-lg-3 border-white border-lg-left">
                <span class="h5 font-weight-bold">Ds <br>
                    9.00h a 12.00h
                </span>
            </div>
        </div>
    </div>
</div>
<div class="position-relative">
    <div class="bg-secondary d-none d-lg-block" style="width: 100%; height: 50%; position: absolute; top: 0; left: 0;"></div>
    <div class="text-center container-only-lg">
        <div class="row no-gutters d-flex justify-content-center align-items-center">

            <div class="col-lg">
                <div id="mapa">
                    <img src="{{ asset('images/mapa.svg') }}" alt="" style="">
                    <a href="#" class="marker"></a>
                    <span class="font-brand-alt font-brand-alt-1 label">Plaça major <br> de Vic</span>
                </div>
            </div>
            <div class="col-4 col-lg-2 order-lg-first pr-lg-2 t-m20">
                <div class="circle-outline ml-auto">
                    <script type="text/javascript">part1='hola@'; part2='mgo.cat'; emailE=(part1 + part2); document.write('<a href="mailto:' + emailE + '" class="d-block inner-text h5 font-weight-bold a-clean">' + part1 + '<br>' + part2 + '</a>');</script>
                </div>
            </div>
            <div class="col-4 col-lg-2 pl-lg-2 t-m20">
                <div class="circle-outline mr-auto">
                    <span class="d-block h5 inner-text font-weight-bold">Tel. <br><a href="tel:+34938866165" class="a-clean">938 866 165</a></span>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="pb-5"></div>
<div class="py-5 bg-gray rounded-top-lg">
    <div class="container">
    @include('inc/form', ['options' => config('site.contact-form-options')])
    </div>
</div>



@endsection

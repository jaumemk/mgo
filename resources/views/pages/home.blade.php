@extends('layouts.main')

@section('main-content')
    <div class="bg-gray rounded-top-lg search-module">
        @include('sections/search', ['parent_id' => 'home-search'])
    </div>

    @if($promotions->count() > 0)
    <div class="pt-4 pb-4 pb-lg-0 bg-light-blue">
        @include('sections/promotions')
    </div>
    @endif

    <div class="@if($promotions->count() > 0) bg-light-blue @else bg-gray @endif">
        @include('sections/properties')
    </div>

    <div class="py-4">
        @include('sections/help')
    </div>

    <div class="py-4 bg-green rounded-top-lg">
        @include('sections/about-us')
    </div>
@endsection

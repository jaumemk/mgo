@extends('layouts.main')
@section('title', $product->pre_title . ' — ' . $product->title . ' — ' . $product->city()->first()->label . ' | ')
@section('main-content')
<div class="bg-gray pt-2 rounded-top">

    <div class="container">

        <ul class="nav nav-pills justify-content-center pt-1" role="tablist">
            <li class="nav-item px-05 px-lg-1" role="presentation">
                <a class="btn btn-nav btn-outline-primary btn-override active" id="pills-photos-tab" data-toggle="pill" href="#pills-photos" role="tab" aria-controls="pills-photos" aria-selected="true">
                    <div class="icon">
                        @php include(public_path('images/icons/icon-camera.svg')) @endphp
                        <span>Fotos</span>
                    </div>
                </a>
            </li>
            @if($product->video)
            <li class="nav-item px-05 px-lg-1" role="presentation">
                <a class="btn btn-nav btn-outline-primary btn-override" id="pills-video-tab" data-toggle="pill" href="#pills-video" role="tab" aria-controls="pills-video" aria-selected="false">
                    <div class="icon">
                        @php include(public_path('images/icons/icon-video.svg')) @endphp
                        <span>Video</span>
                    </div>
                </a>
            </li>
            @endif
            <li class="nav-item px-05 px-lg-1" role="presentation">
                <a class="btn btn-nav btn-outline-primary btn-override" id="pills-360-tab" data-toggle="pill" href="#pills-360" role="tab" aria-controls="pills-360" aria-selected="false">
                    <div class="icon">
                        @php include(public_path('images/icons/icon-360.svg')) @endphp
                        <span>360°</span>
                    </div>
                </a>
            </li>
        </ul>

    </div>

    <div class="tab-content py-3" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-photos" role="tabpanel" aria-labelledby="pills-photos-tab">
            <div id="slider-images">
                @foreach ($product->images as $image)
                <div class="slide">
                    <div class="container">
                        <div class="rounded overflow-hidden">
                            <img src="{{ $image }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="container">
                <div class="row justify-content-center ">
                    <div class="col col-xl-10">
                        <div class="text-center py-2 d-flex justify-content-center">
                            <div class="">
                                <img src="{{ asset('images/arrow-left.svg') }}" alt="" style="width: 20px;">
                            </div>
                            <div class="px-2">
                                <span class="font-brand-alt font-brand-alt-1">Desplaça</span>
                            </div>
                            <div class="">
                                <img src="{{ asset('images/arrow-right.svg') }}" alt="" style="width: 20px;">
                            </div>
                        </div>
                        <div id="slider-images-nav" class="slider">
                            @foreach ($product->images as $image)
                            <div class="item">
                                <div class="rounded-sm overflow-hidden">
                                    <img src="{{ $image }}" alt="" class="img-fluid">
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
            @if($product->video)
            <div class="container">
                <div class="row justify-content-center ">
                    <div class="col">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="{{ $product->video }}" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="tab-pane fade" id="pills-360" role="tabpanel" aria-labelledby="pills-360-tab">
            <div class="container">
                <div class="row justify-content-center ">
                    <div class="col text-center py-5">
                        <div class="py-5 bg-white font-brand-alt font-brand-alt-1">
                            <div class="py-5">
                            Aviat disponible!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="bg-light-blue py-5">
        <div class="container">
            <h1 class="h4 font-weight-bold mb-4 text-center text-lg-left"> {{ $product->pre_title }} {{ ' — ' . $product->city()->first()->label }}</h1>

            <div class="row">
                <div class="col-lg-5 text-center">

                    <div class="row no-gutters font-brand-alt font-brand-alt-1 mb-3 mb-lg-0">
                        <div class="col">
                            Ref.
                            <br>
                            <span class="d-block font-brand-alt-2 mt-1">{{ $product->external_src_reference }}</span>
                        </div>
                        <div class="col border-left border-primary">
                            M<sup class="small font-weight-bold">2</sup>
                            <br>
                            <span class="d-block font-brand-alt-2 mt-1">{{ $product->external_src_area }}</span>
                        </div>
                        <div class="col border-left border-primary">
                            Hab.
                            <br>
                            <span class="d-block font-brand-alt-2 mt-1">{{ $product->external_src_rooms }}</span>
                        </div>
                        <div class="col border-left border-primary">
                            WC
                            <br>
                            <span class="d-block font-brand-alt-2 mt-1">{{ $product->external_src_bathrooms }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 order-lg-first">
                    <div class="content text-center text-lg-left">
                        {{ $product->details }}
                    </div>
                </div>
            </div>
        </div>
        <div class="py-1"></div>
    </div>
    <div class="text-center translate-m50">
        <a href="https://api.whatsapp.com/send?phone=+34938866165&text=Hola, voldria infromació sobre aquest immoble. Referència {{ $product->external_src_reference }}!" class="btn btn-primary btn-override" target="_blank"> Parlem-ne </a>
    </div>
</div>

<div class="py-3 py-lg-5" id="product-specs">
    <div class="container">
        <h2 class="h4 font-weight-bold mb-4"> Característiques </h2>
        <div class="row font-brand-alt font-brand-alt-1">
            <div class="col-12 col-lg-6">

                <div class="dotted-line-h w-100 mb-1"></div>

                @if(is_string(json_decode($product->external_src_object)->LanguageData->Language[1]->txt_AnoConstruccion))
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 pl-md-1">Any de construcció</div>
                    <div class="col-12 col-md-6">{{ json_decode($product->external_src_object)->LanguageData->Language[1]->txt_AnoConstruccion }}</div>
                </div>
                <div class="dotted-line-h w-100 my-1"></div>
                @endif

                @if(json_decode($product->external_src_object)->box_CertificacionEnergeticaEnTramite == 'Disponible')
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 pl-md-1">Certificació energètica</div>
                    <div class="col-12 col-md-6">                    
                        <div class="px-05 text-nowrap">
                            <img src="{{ asset('images/ce/ce-'.json_decode($product->external_src_object)->box_CertificacionEnergetica.'.png' )}}" alt="" style="width: 28px;" class="pr-1">
                            Consum {{ json_decode($product->external_src_object)->box_PrestacionEnergetica }} kwh/m<sup>2</sup> any <br>
                        </div>
                        <div class="px-05 text-nowrap">
                            <img src="{{ asset('images/ce/ce-'.json_decode($product->external_src_object)->box_CertificacionEmisiones.'.png' )}}" alt="" style="width: 28px;" class="pr-1">
                            Emisions {{ json_decode($product->external_src_object)->box_PrestacionEmisiones }} kg CO<sub>2</sub>/m<sup>2</sup> any<br>
                        </div>
                    
                    </div>
                </div>
                @else
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 pl-md-1">Certificació energètica</div>
                    <div class="col-12 col-md-6">
                        {{ json_decode($product->external_src_object)->box_CertificacionEnergeticaEnTramite }}
                    </div>
                </div>
                @endif

                <div class="dotted-line-h w-100 mt-1"></div>

            </div>

            <div class="col-12 col-lg-6">

                <div class="dotted-line-h w-100 mb-1"></div>

                @if(!is_null($product->getExternalTxt('Calefaccion')))
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 pl-md-1">Calefacció</div>
                    <div class="col-6">{{ $product->getExternalTxt('Calefaccion') }}</div>
                </div>
                <div class="dotted-line-h w-100 my-1"></div>
                @endif

                @if(!is_null($product->getExternalTxt('CarpinteriaExterior')))
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 pl-md-1">Carpinteria Exterior</div>
                    <div class="col-12 col-md-6">{{ $product->getExternalTxt('CarpinteriaExterior') }}</div>
                </div>
                <div class="dotted-line-h w-100 my-1"></div>
                @endif

                @if($product->getExternalTxt('CarpinteriaInterior'))
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 pl-md-1">Carpinteria Interior</div>
                    <div class="col-12 col-md-6">{{ $product->getExternalTxt('CarpinteriaInterior') }}</div>
                </div>
                <div class="dotted-line-h w-100 mt-1"></div>
                @endif

                {{--
                <div class="">
                    <div class="row no-gutters">
                        <div class="col-6 pl-1">Etiquetes</div>
                        <div class="col-6">Fletxa</div>
                    </div>
                    <ul id="product-tags" class="list-unstyled pt-2 pl-2">
                        <li>Etiqueta 1</li>
                        <li>Etiqueta 2</li>
                        <li>Etiqueta 3</li>
                    </ul>
                </div>
                <div class="dotted-line-h w-100 my-1"></div>
                --}}

            </div>

        </div>
    </div>
</div>

<div class="py-5 bg-yellow rounded-top-lg">
    <div class="container">
    @include('inc/form', ['options' => config('site.product-form-options')])
    </div>
</div>

@endsection

@extends('layouts.main')
@section('title', 'Cerca immobles |')
@section('main-content')
<div class="bg-green rounded-top-lg search-module has-advanced-filters">
    @include('sections/search', ['advanced' => true, 'parent_id' => 'results'])
</div>
<div class="container">
    <div class="text-center pt-4">
        <h5 class="m-0">{{ count($results) }} {{ Illuminate\Support\Str::plural('resultat', count($results)) }}</h5>
    </div>
    <div id="results" class="row no-gutters pt-4">
        @foreach ($results as $item)
        <div class="result-wrapper col-12 col-lg-4 pb-3">
            <div class="result">
                <div class="result-inner-wrapper pb-3 pb-lg-0">
                    @include('inc/property/article', ['article' => $item])
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

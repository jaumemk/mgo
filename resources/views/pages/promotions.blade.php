@extends('layouts.main')
@section('title', 'Promocions |')
@section('main-content')
    <div class="rounded-top-lg pt-4 bg-light-blue">
        @include('sections/promotions')
    </div>
@endsection

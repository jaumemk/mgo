@extends('layouts.main')
@section('title', 'Contacte |')
@section('main-content')

<div class="rounded-top-lg overflow-hidden">
    <img src="{{ asset('images/nosaltres/header.jpg') }}" alt="" class="img-fluid w-100">
</div>

<section>
    <div class="bg-dark-blue py-2">
        <!-- fake bg -->
    </div>
    <div class="text-center text-white bg-dark-blue" style="background-image: url({{ asset('images/pattern.svg') }}); background-size: 1200px; backround-position: center center;">
        <div class="container">
            <h1 class="display display-4">Qui som?</h1>
            <div class="row justify-content-center py-3">
                <div class="col-lg-6">
                    <p>
                        Som MG Osona, una empresa de Vic que et facilita el procés de trobar l'immoble que desitges, amb la major
                        garantia i tranquil·litat i amb un assessorament continuat i personalitat.
                    </p>
                    <p>
                        Van néixer l'any 2009 amb l'objectiu de ser un referent en el tracte amb les persones, per això tenim el nostre
                        propi model que ens permet oferir-te les millors propietats de la comarca.
                    </p>
                    <p>
                        Ens agrada dir que som persones al servei de persones, ja que sabem que comprar, vendre o llogar un
                        habitatge és una de les decisions més importants en la trajectòria vital d'una persona i per això la nostra
                        finalitat és que aquesta experiència sigui agradable, fàcil, ràpida, entenedora i sobretot transparent per tu,
                        posant en cada pas del camí tot el nostre esforç i coneixement.
                    </p>
                </div>
            </div>
        </div>
        <div class="position-relative">
            <div class="bg-white w-100 position-absolute" style="bottom: 0; height: 50%;">
                <!-- fake bg -->
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                    </div>
                    <div class="col-lg-7">
                        <div class="rounded-lg overflow-hidden">
                            <img src="{{ asset('images/nosaltres/slide-1.jpg') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="py-5 bg-white responsive-margin-1">
        <div class="container">
            <div class="row pt-3">
                <div class="col-lg-4">
                    <p>
                        Ens agrada dir que som persones al servei de persones, ja que sabem que comprar, vendre o llogar un habitatge és una de les decisions més importants en la trajectòria vital d’una persona. Per això, la nostra finalitat és que aquesta experiència sigui agradable, fàcil, ràpida, entenedora i sobretot transparent per tu, posant en cada pas del camí tot el nostre esforç i coneixement. Per aconseguir-ho, contem amb un equip format pels millors professionals.
                    </p>
                </div>
                <div class="col-lg-8">
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="rounded-top-lg overflow-hidden bg-gray pt-5">
        <div class="container text-center">
            <h1 class="h4 font-weight-bold pb-1">El nostre equip</h1>

            <div class="pb-5 text-center">
                <p>
                    Per aconseguir els nostres objectius contem amb un equip format pels millors professionals:
                </p>
            </div>

            <div class="row no-gutters">
                <div class="col-lg px-3 px-lg-5 pb-5">
                    <div class="person">
                        <img src="{{ asset('images/nosaltres/equip-jvalenti.jpg') }}" class="img-fluid rounded-lg" alt="">

                        <a class="social-circle" href="https://api.whatsapp.com/send?phone=+34651990225&text=Hola Valentí!" role="button" target="_blank">
                            <div class="icon">
                                @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                            </div>
                        </a>
                    </div>
                    <h1 class="h4 font-weight-bold pb-1">Josep Valentí</h1>
                    Gerent <br>
                    <script type="text/javascript">
                        emailE = ('valenti@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>
                    | <a href="tel:+34651990225" class="a-clean">651 990 225</a>
                </div>
                <div class="col-lg px-3 px-lg-5 pb-5">
                    <div class="person">
                        <img src="{{ asset('images/nosaltres/equip-dolors.jpg') }}" class="img-fluid rounded-lg" alt="">

                        <a class="social-circle" href="https://api.whatsapp.com/send?phone=+34656706992&text=Hola Dolors!" role="button">
                            <div class="icon">
                                @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                            </div>
                        </a>
                    </div>
                    <h1 class="h4 font-weight-bold pb-1">Dolors Grau</h1>
                    Comptabilitat <br>
                    <script type="text/javascript">
                        emailE = ('dolors@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>
                    | <a href="tel:+34656706992" class="a-clean">656 706 992</a>
                </div>
                <div class="col-lg px-3 px-lg-5 pb-5">
                    <div class="person">
                        <img src="{{ asset('images/nosaltres/equip-nuria.jpg') }}" class="img-fluid rounded-lg" alt="">

                        <a class="social-circle" href="https://api.whatsapp.com/send?phone=+34650734396&text=Hola Núria!" role="button">
                            <div class="icon">
                                @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                            </div>
                        </a>
                    </div>
                    <h1 class="h4 font-weight-bold pb-1">Núria Ramírez</h1>
                    Comercial <br>
                    <script type="text/javascript">
                        emailE = ('nuria@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>

                    | <a href="tel:+34650734396" class="a-clean">650 734 396</a>
                </div>
            </div>

            <div class="row no-gutters">
                <div class="col-lg px-3 px-lg-5 pb-5">
                    <div class="person">
                        <img src="{{ asset('images/nosaltres/equip-maica.jpg') }}" class="img-fluid rounded-lg" alt="">

                        <a class="social-circle" href="https://api.whatsapp.com/send?phone=+34646456847&text=Hola Maica!" role="button">
                            <div class="icon">
                                @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                            </div>
                        </a>
                    </div>
                    <h1 class="h4 font-weight-bold pb-1">Maica Salvans</h1>
                    Comercial <br>
                    <script type="text/javascript">
                        emailE = ('maica@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>

                    | <a href="tel:+34646456847" class="a-clean">646 456 847</a>
                </div>
                <div class="col-lg px-3 px-lg-5 pb-5">
                    <div class="person">
                        <img src="{{ asset('images/nosaltres/equip-gemma.jpg') }}" class="img-fluid rounded-lg" alt="">

                        <a class="social-circle" href="https://api.whatsapp.com/send?phone=+34646456847&text=Hola Gemma!" role="button">
                            <div class="icon">
                                @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                            </div>
                        </a>
                    </div>
                    <h1 class="h4 font-weight-bold pb-1">Gemma Serra</h1>
                    Comercial <br>
                    <script type="text/javascript">
                        emailE = ('gemma@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>

                    | <a href="tel:+34628548061" class="a-clean">628 548 061</a>
                </div>
                <div class="col-lg px-3 px-lg-5 pb-5">
                    <div class="person">
                        <img src="{{ asset('images/nosaltres/equip-ariadna.jpg') }}" class="img-fluid rounded-lg" alt="">

                        <script type="text/javascript">
                            emailE = ('hola@' + 'mgo.cat');
                            document.write('<a href="mailto:' + emailE + '" class="social-circle" role="button">');

                        </script>
                        <div class="icon">
                            @php include(public_path('images/icons/icon-whatsapp.svg')) @endphp
                        </div>
                        </a>
                    </div>
                    <h1 class="h4 font-weight-bold pb-1">Ariadna López</h1>
                    Publicitat <br>
                    <script type="text/javascript">
                        emailE = ('hola@' + 'mgo.cat');
                        document.write('<a href="mailto:' + emailE + '" class="a-clean">' + emailE + '</a>');

                    </script>
                </div>
            </div>


        </div>
    </div>
</section>

<section>
    <div class="bg-black text-white">
        <div class="container responsive-bg-image-1">
            <div class="py-5"><!-- this is just a spacer --></div>
            <div class="d-flex align-items-center">
                <div class="py-3 py-lg-5">
                    <h1 class="text-center text-lg-left">Creiem en la imatge</h1>
                    <div class="row justify-content-center justify-content-lg-start">
                        <div class="col-11 col-lg-6">
                            <div class="pt-2 pt-lg-5 text-center text-lg-left">
                                <p>
                                    Tal com diuen "una imatge val més que mil paraules" i a MGO ho creiem. Per això, la imatge és un dels
                                    nostres serveis més cuidats. Utilitzem càmeres professionals que ens permeten capturar i mostrar al
                                    comprador una història i una il·lusió per visitar el teu habitatge que no podríem aconseguir amb les paraules.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-5"><!-- this is just a spacer --></div>
            <div class="py-5 d-lg-none"><!-- this is just a spacer --></div>
        </div>
    </div>
</section>

<section id="values">
    <div class="bg-black text-white">
        <div class="bg-secondary rounded-top-lg overflow-hidden">
            <div class="container py-5">
                <h1 class="text-center pb-5">Els nostres valors</h1>
                <div class="row justify-content-center">
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-experiencia.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Experiència</h1>
                        <p class="m-0 text-primary">
                            Fa més de 10 anys que actuem en el sector immobiliari, ajudant així a famílies a vendre, llogar i comprar la seva casa.
                        </p>
                    </div>
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-agilitat.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Agilitat</h1>
                        <p class="m-0 text-primary">
                            El nostre equip està format per professionals del sector, cosa que ens permet
                            actuar ràpidament qualsevol dels casos per satisfer les necessitats específiques de cada client.
                        </p>
                    </div>
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-innovacio.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Innovació</h1>
                        <p class="m-0 text-primary">
                            Ens definim com innovadors, ja que adaptem totes les noves tecnologies del mercat al nostre sector per oferir el millor servei.
                        </p>
                    </div>
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-eficacia.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Eficàcia</h1>
                        <p class="m-0 text-primary">
                            L'eficàcia lligada a l'agilitat ens permet oferir un servei 100% personalitzat a cada client.
                        </p>
                    </div>
                </div>
                <div class="pb-lg-5"></div>
                <div class="row justify-content-center">
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-compromis.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Compromís</h1>
                        <p class="m-0 text-primary">
                            El compromís ens permet crear un vincle únic amb els nostres clients.
                        </p>
                    </div>
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-seguretat.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Seguretat</h1>
                        <p class="m-0 text-primary">
                            Gràcies a la nostra experiència podem oferir solucions ràpides i eficaces en cada cas.
                        </p>
                    </div>
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-precisio.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Precisió</h1>
                        <p class="m-0 text-primary">
                            El teu immoble mereix el millor comprador.
                        </p>
                    </div>
                    <div class="col-8 col-lg-3 text-center pb-3">
                        <div class="icon value pb-2">
                            @php include(public_path('images/icons/valors-constancia.svg')) @endphp
                        </div>
                        <h1 class="h5 font-brand-alt font-brand-alt-1 pb-1">Constància</h1>
                        <p class="m-0 text-primary">
                            Superem els obstacles que puguem trobar en el nostre dia a dia i en cada cas amb fermesa i seguretat.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

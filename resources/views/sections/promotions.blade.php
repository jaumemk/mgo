<section>
    @if(Route::currentRouteName() == 'home')
    <div class="text-center mb-1">
        <h1 class="sr-only">Promocions</h1>
        <a href="{{ route('promotions') }}" class="btn btn-primary btn-override">Promocions</a>
    </div>
    @endif
    <div class="container pt-3">
        <div class="row no-gutters">

            @foreach ($promotions as $promotion)
            <div class="col-12 col-lg-5 mb-lg-5">
                @include('inc/promotion/article', compact('promotion'))
            </div>

            {{-- TODO: here starts a quick vertical and horitzonatl lines workarround --}}
            @if(!$loop->last)
            <div class="d-lg-none col-12 col-lg-2 mb-lg-5">
                <div class="d-flex justify-content-center h-100">
                    <div class="dotted-line-h d-lg-none mt-2 mb-3 w-100"></div>
                    <div class="dotted-line-v d-none d-lg-block"></div>
                </div>
            </div>
            @endif
            @if($loop->odd)
            <div class="d-none d-lg-block col-12 col-lg-2 mb-lg-5">
                <div class="d-flex justify-content-center h-100">
                    <div class="dotted-line-h d-lg-none mt-2 mb-3 w-100"></div>
                    <div class="dotted-line-v d-none d-lg-block"></div>
                </div>
            </div>
            @endif
            {{-- TODO: here ends quick vertical and horitzonatl lines workarround --}}

            
            @endforeach

        </div>
    </div>
</section>

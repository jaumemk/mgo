<section id="properties">
    <div class="pt-4 pb-5 bg-yellow rounded-top-lg">
        <div class="text-center">
            <h1 class="display display-4">Destacats</h1>
        </div>
        <div class="text-center py-3 d-flex justify-content-center">
            <div class="">
                <img src="{{ asset('images/arrow-left.svg') }}" alt="" style="width: 20px;">
            </div>
            <div class="px-2">
                <span class="font-brand-alt font-brand-alt-1">Desplaça</span>
            </div>
            <div class="">
                <img src="{{ asset('images/arrow-right.svg') }}" alt="" style="width: 20px;">
            </div>
        </div>
        <div class="container">
            <div id="slider" class="slider">
                @foreach (\App\Models\Product::where('highlighted', 1)->get() as $item)
                <div class="item">
                    @include('inc/property/article', ['article' => $item, 'class' => 'btn-white'])
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section>
    <form action="{{ route('filter') }}" method="get">
        <div class="container py-4 font-brand-alt font-brand-alt-1">
            {{-- main switch --}}
            <div class="row">
                <div class="col d-none d-lg-block"></div>
                <div class="col">
                    <div class="text-center custom-control custom-switch custom-double-switch">
                        <input type="checkbox" name="mode" value="rent" class="custom-control-input" id="{{$parent_id}}-mode-switch" @if(!is_null(request()->input('mode')) && (request()->input('mode') == 'rent')) checked @endif>
                        <label class="pre-label" for="{{$parent_id}}-mode-switch">Comprar</label>
                        <label class="custom-control-label" for="{{$parent_id}}-mode-switch">Llogar</label>
                    </div>
                </div>
                <div class="col d-none d-lg-block text-right">
                    <button type="submit" class="btn btn-primary btn-override">
                        <span class="search-btn-text-option-1">Cercar</span>
                        <span class="search-btn-text-option-2">Aplica els filtres</span>
                    </button>
                </div>
            </div>
            <div class="py-3 pt-lg-3 pb-lg-0">
                <div class="row pb-lg-0">

                    <div class="col-12 col-lg-4">
                        <div class="mgo-filter">

                            <div class="mgo-filter-wrapper has-filters">
                                <span class="dynamic-label">Estic buscant</span>
                                <div class="filters">
                                    <div class="filters-inner-wrapper d-flex align-content-start flex-wrap">
                                        @foreach ( \App\Models\Type::all() as $type )
                                        <div class="filter-item w-50 custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="{{$parent_id}}-type-{{ $type->slug }}-switch" name="types[]" value="{{ $type->id }}" @if(is_array(request()->input('types')) && in_array($type->id,request()->input('types'))) checked @endif
                                            >
                                            <label class="custom-control-label" for="{{$parent_id}}-type-{{ $type->slug }}-switch">{{ $type->label }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-4">
                        <div class="mgo-filter">
                            <div class="mgo-filter-wrapper has-filters">
                                <span class="dynamic-label">A la població de</span>
                                <div class="filters">
                                    <div class="filters-inner-wrapper">
                                        @foreach ( \App\Models\City::all() as $location )
                                        <div class="filter-item custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="{{$parent_id}}-type-{{ $location->slug }}-switch" name="cities[]" value="{{ $location->id }}" @if(is_array(request()->input('cities')) && in_array($location->id,request()->input('cities'))) checked @endif
                                            >
                                            <label class="custom-control-label" for="{{$parent_id}}-type-{{ $location->slug }}-switch">{{ $location->label }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-4">
                        <div class="mgo-filter">
                            <div class="mgo-filter-wrapper">
                                <div class="d-flex">
                                    <span class="dynamic-label">Referència</span> <input type="text" name="ref" value="{{ request()->input('ref') }}" class="input-simple" placeholder="Núm. #" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                @if(isset($advanced) && $advanced)
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="mgo-filter">
                            <div class="mgo-filter-wrapper">
                                <div class="d-flex">
                                    <span class="dynamic-label">Superfície&nbsp;M<sup>2</sup></span> <input type="text" name="min-area" value="" class="input-simple" placeholder="Min" />
                                </div>
                            </div>
                        </div>
                        <div class="mgo-filter">
                            <div class="mgo-filter-wrapper has-filters">
                                <span class="dynamic-label">Característiques</span>
                                <div class="filters">
                                    <div class="filters-inner-wrapper d-flex align-content-start flex-wrap">
                                        @foreach ( config('site.extras') as $key => $extra )
                                        <div class="filter-item w-50 custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="type-{{ $key }}-switch" name="extras[]" value="{{ $key }}" @if(is_array(request()->input('extras')) && in_array($key,request()->input('extras'))) checked @endif
                                            >
                                            <label class="custom-control-label" for="type-{{ $key }}-switch">{{ $extra[1] }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="mgo-filter">
                            <div class="mgo-filter-wrapper">
                                <span class="dynamic-label thin">Habitacions</span>
                                <div class="btn-group-toggle-wrapper text-right">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-room" value="room-1" id="{{$parent_id}}-option-room-1" autocomplete="off" @if(request()->input('option-room') == 'room-1') checked @endif> &nbsp; 1+ &nbsp;
                                        </label>
                                        <span class="d-lg-none d-xl-inline">&nbsp;&nbsp;</span>
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-room" value="room-2" id="{{$parent_id}}-option-room-2" autocomplete="off" @if(request()->input('option-room') == 'room-2') checked @endif> &nbsp; 2+ &nbsp;
                                        </label>
                                        <span class="d-lg-none d-xl-inline">&nbsp;&nbsp;</span>
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-room" value="room-3" id="{{$parent_id}}-option-room-3" autocomplete="off" @if(request()->input('option-room') == 'room-3') checked @endif> &nbsp; 3+ &nbsp;
                                        </label>
                                        <span class="d-lg-none d-xl-inline">&nbsp;&nbsp;</span>
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-room" value="room-4" id="{{$parent_id}}-option-room-4" autocomplete="off" @if(request()->input('option-room') == 'room-4') checked @endif> &nbsp; 4+ &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="mgo-filter">
                            <div class="mgo-filter-wrapper">
                                <span class="dynamic-label thin">Banys</span>
                                <div class="btn-group-toggle-wrapper text-right">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-bathroom" value="bathroom-1" id="{{$parent_id}}-option-bathroom-1" autocomplete="off" @if(request()->input('option-bathroom') == 'bathroom-1') checked @endif> &nbsp; 1+ &nbsp;
                                        </label>
                                        <span class="d-lg-none d-xl-inline">&nbsp;&nbsp;</span>
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-bathroom" value="bathroom-2" id="{{$parent_id}}-option-bathroom-2" autocomplete="off" @if(request()->input('option-bathroom') == 'bathroom-2') checked @endif> &nbsp; 2+ &nbsp;
                                        </label>
                                        <span class="d-lg-none d-xl-inline">&nbsp;&nbsp;</span>
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-bathroom" value="bathroom-3" id="{{$parent_id}}-option-bathroom-3" autocomplete="off" @if(request()->input('option-bathroom') == 'bathroom-3') checked @endif> &nbsp; 3+ &nbsp;
                                        </label>
                                        <span class="d-lg-none d-xl-inline">&nbsp;&nbsp;</span>
                                        <label class="btn btn-outline-advanced-filter">
                                            <input type="radio" name="option-bathroom" value="bathroom-4" id="{{$parent_id}}-option-bathroom-4" autocomplete="off" @if(request()->input('option-bathroom') == 'bathroom-4') checked @endif> &nbsp; 4+ &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            </div>
            <div class="d-lg-none">
                <button type="submit" class="btn btn-primary btn-override btn-block">
                    <span class="search-btn-text-option-1">Cercar</span>
                    <span class="search-btn-text-option-2">Aplica els filtres</span>
                </button>
            </div>
        </div>
    </form>
</section>

<section>
    <h1 class="display display-4 text-center mb-4">Obrint—vos <br> portes</h1>
    <div class="container text-center">
        <div class="row justify-content-center">
            <div class="col-10 col-lg-8">
                <p>
                    T’imagines fer-te amb la clau de la teva nova llar? Nosaltres t’hi ajudem! T’informem de tot el que necessites saber abans de comprar o llogar.
                </p>
            </div>
        </div>

        <div class="my-3 row justify-content-center">
            <div class="col col-lg-5">
                <div class="row justify-content-center">
                    <div class="col-6 pr-05">
                        <a class="btn btn-block btn-primary btn-override text-nowrap" href="#" role="button">Guia compradors</a>
                    </div>
                    <div class="col-6 pl-05">
                        <a class="btn btn-block btn-primary btn-override text-nowrap" href="#" role="button">Guia llogaters</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-10 col-lg-8">
                <p>
                    Si necessites informació o tens qualsevol dubte, <br class="d-none d-lg-block"> estarem encantats d’atendre’t.
                </p>
                <a class="btn btn-link btn-override" href="#" role="button">Anar al fomulari</a>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="text-center">
        <h1 class="display display-4">Som MGO</h1>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-lg-6">
                <div class="text-center pt-4">
                    <p>
                        A MG sabem que comprar o llogar un habitatge és una de les decisions més importants en
                        la trajectòria vital d’una persona i per això, el nostre objectiu és que aquesta experiència
                        sigui lo més agradable, fàcil, ràpida, entenedora i sobretot transparent per tu,
                        posant en cada pas que fem tot el nostre esforç i coneixement.
                    </p>
                </div>
            </div>
        </div>

        <div class="my-3 row justify-content-center">
            <div class="col col-lg-4">
                <div class="row justify-content-center">
                    <div class="col-6 pr-05">
                        <a class="btn btn-block btn-primary btn-override-2" href="{{ route('contact') }}" role="button">Serveis</a>
                    </div>
                    <div class="col-6 pl-05">
                        <a class="btn btn-block btn-primary btn-override-2" href="{{ route('about-us') }}" role="button">Nosaltres</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

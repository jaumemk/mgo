<header id="navigation-wrapper">
    <nav id="navigation" class="">
        <div id="navigation-header">

            <div class="container">
                <div class="row no-gutters">
                    <div class="col"></div>
                    <div class="col">
                        <a href="{{ route('home') }}" class="d-block"><img id="logo" src="{{ asset('images/logo.svg') }}" alt=""></a>
                    </div>
                    <div class="col">

                        <div id="burger-toggler-triggerer" class="text-center">
                            <a id="navigation-toggler" class="burger d-inline-block">
                                <span class="sr-only">Open menu</span>
                            </a>
                            <span class="label font-brand-alt font-brand-alt-1">Menú</span>
                            <span class="label font-brand-alt font-brand-alt-1 text-white">Tancar</span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div id="navigation-contents" class="d-flex align-items-center container">
            <ul class="list-unstyled text-center mb-0 w-100">
                <li class="d-lg-none">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="">
                    <a href="{{ route('filter') }}">Immobles</a>
                </li>
                <li class="">
                    <a href="{{ route('promotions') }}">Promocions</a>
                </li>
                {{--
                <li class="">
                    <a href="#">Guia compradors</a>
                </li>
                <li class="">
                    <a href="#">Guia llogaters</a>
                </li> --}}
                {{-- <li class="">
                    <a href="#">Serveis MG</a>
                </li> --}}
                <li class="">
                    <a href="{{ route('about-us') }}">Nosaltres</a>
                </li>
                <li class="">
                    <a href="{{ route('contact') }}">Contacte</a>
                </li>
            </ul>
        </div>
        <div id="navigation-extra" class="text-white">
            <div class="text-center">

            </div>
        </div>
    </nav>
</header>

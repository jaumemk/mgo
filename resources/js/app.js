// import AOS from 'aos';
import * as bootstrap from 'bootstrap';
import slick from 'slick-carousel';

$(function () {

    AOS.init();

    $('#slider').slick({
        mobileFirst: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        adaptiveHeight: true,
        //variableWidth: true,
        //centerMode: true,
        responsive: [{
            breakpoint: 992, // large
            settings: {
                slidesToShow: 3,
                arrows: true,
            }
        }]
    });

    $('#slider-images').slick({
        mobileFirst: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        adaptiveHeight: true,
        infinite: false,
        asNavFor: '#slider-images-nav',
        responsive: [{
            breakpoint: 992, // large
            settings: {
                slidesToShow: 1,
                centerMode: true,
                centerPadding: 'calc(50vw - 475px)'
            }
        }]
    });

    $('#slider-images-nav').slick({
        mobileFirst: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        adaptiveHeight: true,
        centerMode: false,
        infinite: false,
        asNavFor: '#slider-images',
        focusOnSelect: true,
        responsive: [{
            breakpoint: 992, // large
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1,
                arrows: true,
            }
        }]
    });

    // $('#slider-images-nav .slick-slide').on('click', function() {
    //     $('#slider-images').slick('slickGoTo',$(this).attr('data-slick-index'));
    // });

    $('#burger-toggler-triggerer').on('click', function () {
        $('#navigation').toggleClass('active');
        $('#social-float').toggleClass('navigation-active');
    });

    $('.mgo-filter-wrapper.has-filters .dynamic-label').on('click', function () {
        $(this).parent().toggleClass('active');
    });
    
    $('#show-full-search, #close-full-search').on('click', function(){
        $('#full-search').toggleClass('active');
        $('#social-float').toggleClass('fullsearch-active');
    });

});

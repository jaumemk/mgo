<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \Artisan::call('product:refresh');

        \App\Models\User::create([
            'name' => 'Jaume',
            'email' => 'jaumemk@gmail.com',
            'password' => Hash::make('barcelona'),
        ]);
    }
}

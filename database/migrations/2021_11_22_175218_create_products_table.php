<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('mode')->nullable();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->boolean('highlighted')->nullable();
            $table->json('extras')->nullable();
            $table->string('external_src_reference')->nullable();
            $table->float('external_src_price', 12, 2)->nullable();
            $table->float('external_src_area', 12, 2)->nullable();
            $table->integer('external_src_rooms')->nullable();
            $table->integer('external_src_bathrooms')->nullable();
            $table->json('external_src_object');
            $table->integer('promotion_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

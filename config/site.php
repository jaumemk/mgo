<?php

return [

    'extras' => 
    [
        'terrasse' => ['box_Terraza', 'Terrassa'],
        'parking' => ['box_Garaje', 'Garatge'],
        'elevator' => ['box_Ascensor', 'Ascensor'],
        'heating' => ['box_Calefaccion', 'Calefacció'],
        'fourniture' => ['box_Muebles', 'Moblat']
    ],

    'product-form-options' => [
        'saber-mes' => 'Saber més d\'aquest immoble',
        'comprar-immoble' => 'Comprarlo',
        'agendar' => 'Agendar una visita',
        'altres' => 'Altres',
    ],

    'contact-form-options' => [
        'saber-mes' => 'Saber més d\'un immoble',
        'comprar-immoble' => 'Comprar',
        'llogar-immoble' => 'Llogar',
        'agendar' => 'Agendar una visita',
        'altres' => 'Altres',
    ]

];
